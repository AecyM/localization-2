# Localization Packages #

This repository is used to share localization data for ArmaServicesNET products.

### How to install the package? ###

* Read BIS official guide on how to use [stringtable.xml](https://community.bistudio.com/wiki/Stringtable.xml) in your addons/mission file.
* Include a new project into your **stringtable.xml** with a name **ASN** or copy it with your first desired package within the provided files.
* Pick the desired **Package** and add it to your **ASN** project you added to your **stringtable.xml** in a step above
* Add your **stringtable.xml** into your mission/addon.

### How to contribute? ###

* Clone a repository
* Make changes
* Make a **pull request**.

### Contribution Rules ###

* Respect other authors in your work, if your work interesects with other author translation and/or modifies it - make a note in the code and **pull requests**:
```XML
	<!-- English Translation: OtherAuthor, OtherAuthor2, YourNickname-->
	<Project name="ASN">
    	<Package name="Module Name">
		...
	...
```
* Do not post unnecessary data, re-used data or bad localization data - this request will be rejected immediately.
* Keep string format to **STR_ASN_<modulename>_<stringname>** format. Here is an example:
```XML
	    <Key ID="STR_ASN_PERKS_PERKS">
			<English>Perks</English>
            <Russian>Навыки</Russian>
        </Key>
```
* Post only genuine work, do not copypaste someone else work. 